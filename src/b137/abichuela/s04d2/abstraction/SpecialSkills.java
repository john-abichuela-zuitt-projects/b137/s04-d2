package b137.abichuela.s04d2.abstraction;

public interface SpecialSkills {

    default void computerProgram() {
        System.out.println("I can program in Java!");
    };

    default void driveACar() {
        System.out.println("I can drive a car!");
    };
}
