package b137.abichuela.s04d2.abstraction;

public class Person implements Actions, SpecialSkills{

    public Person() {}

    // Methods from Actions interface
    public void sleep() {
        Actions.super.sleep();
    }

    public void run() {
        Actions.super.run();
    }

    // Methods from SpecialSkills interface
    public void computerProgram() {
        SpecialSkills.super.computerProgram(); // Invoked from SpecialSkills interface
    }

    public void driveACar() {
        SpecialSkills.super.driveACar();
    }
}
