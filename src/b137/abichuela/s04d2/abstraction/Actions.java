package b137.abichuela.s04d2.abstraction;

public interface Actions {
    default void sleep() {
        System.out.println("Zzzz...");
    };
    default void run () {
        System.out.println("Running...");
    };
}
