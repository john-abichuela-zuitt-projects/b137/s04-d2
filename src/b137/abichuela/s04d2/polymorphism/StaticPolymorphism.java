package b137.abichuela.s04d2.polymorphism;

public class StaticPolymorphism {

    public int add(int a, int b) {
        return a + b;
    }

    // Overload the add method by requiring another parameter
    public int add(int a, int b, int c) {
        return a + b + c;
    }

    // Overload by changing the data type
    public double add(double a, double b) {
        return a + b;
    }
}
